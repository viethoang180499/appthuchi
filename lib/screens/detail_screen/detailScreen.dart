import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/screens/add_edit_screen.dart/add_edit_screen.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widget/detail_screen_widget.dart';

class DetailScreen extends StatelessWidget {
  final String exchangeId;
  DetailScreen({@required this.exchangeId});

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExchangesBloc, ExchangesState>(
      builder: (BuildContext context, state) {
        final exchange = (state as ExchangesLoadSuccess)
            .exchanges
            .firstWhere((exchange) => exchange.id == exchangeId);
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text(BlocProvider.of<LocalizationBloc>(context)
                .state
                .detailScreenTitle),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.border_color,
                  //color: Colors.black,
                ),
                onPressed: () async {
                  final saved = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddEditScreen(
                                isEditing: true,
                                exchange: exchange,
                              )));
                  if (saved != null) {
                    Helper().displaySaveSuccessSnackbar(context, _scaffoldKey);
                  }
                },
              ),
              IconButton(
                onPressed: () {
                  BlocProvider.of<ExchangesBloc>(context)
                      .add(ExchangeDeleted(exchange: exchange));
                  Navigator.pop(context, 'Deleted exchange');
                },
                icon: Icon(
                  Icons.delete,
                  //color: Colors.black,
                ),
              ),
            ],
          ),
          body: Column(
            children: <Widget>[
              SizedBox(
                height: 200,
                child: exchange.image != '' && exchange.image != null
                    ? Image.network(
                        exchange.image,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                      )
                    : Center(
                        child: Container(
                          width: 150,
                          height: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey[200],
                          ),
                          child: Center(
                              child: Text(
                                  BlocProvider.of<LocalizationBloc>(context)
                                      .state
                                      .noImage)),
                        ),
                      ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 0),
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Name(name: exchange.name),
                        Category(categoryId: exchange.categoryId),
                        Date(date: exchange.date),
                        Expanded(
                          child: Note(note: exchange.note),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Amount(
                    amount: exchange.amount,
                    exchangeType: exchange.exchangeType),
              ),
            ],
          ),
        );
      },
    );
  }
}
