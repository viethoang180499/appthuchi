import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';

class Date extends StatelessWidget {
  final DateTime date;

  const Date({Key key, @required this.date}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        Icons.calendar_today,
        color: Colors.black,
      ),
      title: Text(Helper().datetimeToDay(date)),
      trailing: Text(Helper().datetimeToTime(date)),
    );
  }
}
