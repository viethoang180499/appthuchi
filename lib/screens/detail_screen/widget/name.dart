import 'package:flutter/material.dart';

class Name extends StatelessWidget {
  final String name;

  const Name({Key key, @required this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        Icons.assignment,
        color: Colors.black,
      ),
      title: Text(
        '$name',
        style: TextStyle(
          fontSize: 20,
        ),
      ),
    );
  }
}
