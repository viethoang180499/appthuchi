import 'package:flutter/material.dart';

class Note extends StatelessWidget {
  final String note;

  const Note({Key key, @required this.note}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        Icons.note,
        color: Colors.black,
      ),
      title: Text(
        note,
        style: TextStyle(
          fontSize: 16,
          color: Colors.grey,
        ),
      ),
    );
  }
}
