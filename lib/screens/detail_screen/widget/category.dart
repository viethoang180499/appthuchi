import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Category extends StatelessWidget {
  final String categoryId;

  const Category({Key key, @required this.categoryId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    print(categoryId);
    return ListTile(
      leading: Icon(
        Icons.category,
        color: Colors.black,
      ),
      title: Text(
        (BlocProvider.of<CategorysBloc>(context).state as CategorysLoadSuccess)
            .categorys
            .firstWhere((cate) => cate.id == categoryId)
            .name,
        style: TextStyle(
          fontSize: 20,
          color: Colors.grey[700],
        ),
      ),
      trailing: Icon(
        Icons.arrow_right,
        size: 30,
        color: Colors.black,
      ),
    );
  }
}
