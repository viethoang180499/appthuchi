import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class Amount extends StatelessWidget {
  final int amount;
  final ExchangeType exchangeType;
  const Amount({Key key, @required this.amount, @required this.exchangeType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
            child: Text(
              BlocProvider.of<LocalizationBloc>(context).state.amountLable,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.monetization_on,
              color: Colors.blue,
            ),
            trailing: exchangeType == ExchangeType.income
                ? Text(
                    '+${NumberFormat("#,###").format(amount)} ${BlocProvider.of<LocalizationBloc>(context).state.currency}',
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.green,
                    ),
                  )
                : Text(
                    '-${NumberFormat("#,###").format(amount)} ${BlocProvider.of<LocalizationBloc>(context).state.currency}',
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.red,
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
