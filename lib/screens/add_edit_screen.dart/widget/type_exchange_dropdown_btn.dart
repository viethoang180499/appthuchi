import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TypeExchangeDropdownBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
      builder: (BuildContext context, state) => DropdownButton(
        value: state.exchangeType ?? ExchangeType.income,
        items: [
          DropdownMenuItem(
            child: Row(
              children: <Widget>[
                Icon(Icons.mood),
                SizedBox(
                  width: 5,
                ),
                Text(
                  BlocProvider.of<LocalizationBloc>(context).state.incomes,
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
              ],
            ),
            value: ExchangeType.income,
          ),
          DropdownMenuItem(
            child: Row(
              children: <Widget>[
                Icon(Icons.mood_bad),
                SizedBox(
                  width: 5,
                ),
                Text(
                  BlocProvider.of<LocalizationBloc>(context).state.expenses,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
            value: ExchangeType.expense,
          )
        ],
        onChanged: (value) => BlocProvider.of<UpdateExchangeBloc>(context)
            .add(ValueExchangeChanged(exchangeType: value)),
      ),
    );
  }
}
