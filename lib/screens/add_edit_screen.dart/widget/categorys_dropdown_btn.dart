import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategorysDropdownBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategorysBloc, CategorysState>(
      builder: (context, state) {
        List<DropdownMenuItem> items = [];
        final categorys = (state as CategorysLoadSuccess).categorys;
        for (var cate in categorys) {
          items.add(
            DropdownMenuItem<String>(
              child: Text(cate.name),
              value: cate.id,
            ),
          );
        }
        return BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
          builder: (BuildContext context, UpdatedExchangeState state) =>
              DropdownButton(
            items: items,
            value: state.categoryId,
            isExpanded: true,
            onChanged: (item) => BlocProvider.of<UpdateExchangeBloc>(context)
                .add(ValueExchangeChanged(categoryId: item)),
          ),
        );
      },
    );
  }
}
