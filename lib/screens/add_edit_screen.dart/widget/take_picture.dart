import 'dart:io';
import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

class TakePicture extends StatefulWidget {
  @override
  _TakePictureState createState() => _TakePictureState();
}

class _TakePictureState extends State<TakePicture> {
  File _imageFile;

  Future<void> _getImage(BuildContext context, bool useCamera) async {
    var imageFile = useCamera
        ? await ImagePicker.pickImage(source: ImageSource.camera)
        : await ImagePicker.pickImage(source: ImageSource.gallery);

    String filePath = imageFile.path;
    BlocProvider.of<UpdateExchangeBloc>(context)
        .add(ValueExchangeChanged(image: filePath));

    setState(() {
      _imageFile = imageFile;
    });
  }

  Widget image() {
    if (_imageFile == null)
      return Center(
        child: Text(BlocProvider.of<LocalizationBloc>(context).state.noImage),
      );
    else
      return Image.file(
        _imageFile,
        scale: 1,
      );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(top: 10),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                //padding: EdgeInsets.symmetric(horizontal: 5),
                child: SizedBox(
                  height: 200,
                  child: BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
                    builder: (context, state) {
                      if (state is ExchangeLoading) {
                        return CircularProgressIndicator();
                      } else {
                        return (state.image == null || state.image == '')
                            ? Center(
                                child: Container(
                                  width: 150,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.grey[200],
                                  ),
                                  child: Center(
                                      child: Text(
                                          BlocProvider.of<LocalizationBloc>(
                                                  context)
                                              .state
                                              .noImage)),
                                ),
                              )
                            : IntrinsicWidth(
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.center,
                                      child: _imageFile != null
                                          ? image()
                                          : Image.network(
                                              state.image,
                                              loadingBuilder:
                                                  (BuildContext context,
                                                      Widget child,
                                                      ImageChunkEvent
                                                          loadingProgress) {
                                                if (loadingProgress == null)
                                                  return child;
                                                return Center(
                                                    child:
                                                        CircularProgressIndicator());
                                              },
                                            ),
                                    ),
                                    Container(
                                      child: GestureDetector(
                                        child: Icon(
                                          Icons.cancel,
                                          color: Colors.red[400],
                                        ),
                                        onTap: () {
                                          setState(() {
                                            _imageFile = null;
                                            BlocProvider.of<UpdateExchangeBloc>(
                                                    context)
                                                .add(ValueExchangeChanged(
                                                    image: ''));
                                          });
                                        },
                                      ),
                                      alignment: Alignment.topRight,
                                    ),
                                  ],
                                ),
                              );
                      }
                    },
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.add_a_photo),
                      onPressed: () => _getImage(context, true)),
                  IconButton(
                      icon: Icon(Icons.add_photo_alternate),
                      onPressed: () => _getImage(context, false)),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
