import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DateTimePicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
      builder: (BuildContext context, UpdatedExchangeState state) => Row(
        children: <Widget>[
          Container(
            child: Icon(Icons.calendar_today),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: GestureDetector(
              child: Text(
                Helper().datetimeToDay(state.date),
              ),
              onTap: () async {
                var result = state.date;
                final date = await showDatePicker(
                    context: context,
                    initialDate: state.date,
                    firstDate: DateTime(2015, 8),
                    lastDate: DateTime(2101));
                final currentDatetime = state.date;
                result = date != null
                    ? DateTime(
                        date.year,
                        date.month,
                        date.day,
                        currentDatetime.hour,
                        currentDatetime.minute,
                      )
                    : state.date;
                BlocProvider.of<UpdateExchangeBloc>(context)
                    .add(ValueExchangeChanged(date: result));
              },
            ),
          ),
          GestureDetector(
            child: Text(
              Helper().datetimeToTime(state.date),
            ),
            onTap: () async {
              var result = state.date;
              final time = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.fromDateTime(state.date));
              final currentDatetime = state.date;
              result = time != null
                  ? DateTime(
                      currentDatetime.year,
                      currentDatetime.month,
                      currentDatetime.day,
                      time.hour,
                      time.minute,
                    )
                  : state.date;
              BlocProvider.of<UpdateExchangeBloc>(context)
                  .add(ValueExchangeChanged(date: result));
            },
          ),
        ],
      ),
    );
  }
}
