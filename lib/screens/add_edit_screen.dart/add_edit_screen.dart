import 'dart:io';
import 'package:flutter/services.dart';
import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/models/category.dart';
import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/screens/add_edit_screen.dart/widget/add_edit_screen_widget.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart' as path;

class AddEditScreen extends StatefulWidget {
  static const routeName = '/addEditScreen';
  final bool isEditing;
  final Exchange exchange;

  const AddEditScreen({Key key, @required this.isEditing, this.exchange})
      : super(key: key);
  @override
  _AddEditScreenState createState() => _AddEditScreenState();
}

class _AddEditScreenState extends State<AddEditScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _note;
  String _name;
  int _amount;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Exchange get exchange => widget.exchange;
  bool get isEditing => widget.isEditing;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UpdateExchangeBloc(
          exchangesBloc: BlocProvider.of<ExchangesBloc>(context))
        ..add(LoadExchange(exchange != null ? exchange.id : null)),
      child: BlocBuilder<UpdateExchangeBloc, UpdatedExchangeState>(
          builder: (context, state) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: exchange != null
                ? Text(BlocProvider.of<LocalizationBloc>(context)
                    .state
                    .editScreenTitle)
                : Text(BlocProvider.of<LocalizationBloc>(context)
                    .state
                    .addScreenTitle),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.save),
                onPressed: () async {
                  showDialog(
                      context: context,
                      builder: (context) =>
                          Center(child: CircularProgressIndicator()));
                  final onSave = await _onPressSaveBtn(context);
                  if (onSave == 0) {
                    Navigator.pop(context, 'saved');
                  } else {
                    Navigator.pop(context);
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: Duration(seconds: 1),
                      content: Row(
                        children: <Widget>[
                          Icon(Icons.error),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: Text(onSave == -1
                                ? 'Some thing went wrong'
                                : 'No Internet'),
                          ),
                        ],
                      ),
                      backgroundColor: Colors.red,
                    ));
                  }
                },
              ),
            ],
          ),
          body: (state is ExchangeLoading)
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : SingleChildScrollView(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          initialValue: state.name,
                          maxLines: null,
                          decoration: InputDecoration(
                            labelText:
                                BlocProvider.of<LocalizationBloc>(context)
                                    .state
                                    .nameLable,
                          ),
                          onEditingComplete: () {
                            _formKey.currentState.save();
                            BlocProvider.of<UpdateExchangeBloc>(context)
                                .add(ValueExchangeChanged(name: _name));
                          },
                          onSaved: (value) => _name = value,
                          validator: (value) => value.isEmpty
                              ? BlocProvider.of<LocalizationBloc>(context)
                                  .state
                                  .validate
                              : null,
                        ),
                        TextFormField(
                          initialValue: state.note,
                          maxLines: null,
                          maxLength: 200,
                          decoration: InputDecoration(
                            labelText:
                                BlocProvider.of<LocalizationBloc>(context)
                                    .state
                                    .noteLable,
                          ),
                          onEditingComplete: () {
                            _formKey.currentState.save();
                            BlocProvider.of<UpdateExchangeBloc>(context)
                                .add(ValueExchangeChanged(note: _note));
                          },
                          onSaved: (value) => _note = value,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: DateTimePicker(),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.category),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: CategorysDropdownBtn(),
                            ),
                            IconButton(
                                icon: Icon(Icons.add_circle_outline),
                                onPressed: _showDialog),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: TypeExchangeDropdownBtn(),
                        ),
                        TextFormField(
                          style: TextStyle(
                            color: state.exchangeType == ExchangeType.income
                                ? Colors.green
                                : Colors.red,
                          ),
                          initialValue: state.amount.toString(),
                          keyboardType: TextInputType.phone,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          decoration: InputDecoration(
                            icon: Icon(
                              Icons.attach_money,
                              color: Colors.black,
                            ),
                            suffix: Text(
                              BlocProvider.of<LocalizationBloc>(context)
                                  .state
                                  .currency,
                              style: TextStyle(
                                color: state.exchangeType == ExchangeType.income
                                    ? Colors.green
                                    : Colors.red,
                              ),
                            ),
                            hintText: '0',
                            labelText:
                                BlocProvider.of<LocalizationBloc>(context)
                                    .state
                                    .amountLable,
                          ),
                          onEditingComplete: () {
                            _formKey.currentState.save();
                            BlocProvider.of<UpdateExchangeBloc>(context)
                                .add(ValueExchangeChanged(amount: _amount));
                          },
                          onSaved: (value) => _amount = int.parse(value),
                        ),
                        TakePicture(),
                      ],
                    ),
                  ),
                ),
        );
      }),
    );
  }

  void _showDialog() async {
    TextEditingController _cateController = TextEditingController();
    await showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextFormField(
                controller: _cateController,
                autofocus: true,
                decoration: new InputDecoration(
                    labelText: BlocProvider.of<LocalizationBloc>(context)
                        .state
                        .addCateLable,
                    hintText: 'ex. Shopping'),
              ),
            )
          ],
        ),
        actions: <Widget>[
          FlatButton(
              child: Text(
                  BlocProvider.of<LocalizationBloc>(context).state.okBtnText),
              onPressed: () {
                if (_cateController.text != '') {
                  BlocProvider.of<CategorysBloc>(context).add(CategoryAdded(
                      category: Category(name: _cateController.text)));
                  Navigator.pop(context);
                  Helper().displaySaveSuccessSnackbar(context, _scaffoldKey);
                }
              }),
          FlatButton(
              child: Text(BlocProvider.of<LocalizationBloc>(context)
                  .state
                  .cancelBtnText),
              onPressed: () {
                Navigator.pop(context);
              }),
        ],
      ),
    );
  }

  Future<int> _onPressSaveBtn(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return -2;
    }
    final state = BlocProvider.of<UpdateExchangeBloc>(context).state;
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final image = await uploadImage(state.image);
      try {
        if (isEditing) {
          BlocProvider.of<UpdateExchangeBloc>(context).add(
            SubmitedChanges(
                exchange: exchange.copyWith(
                    name: _name,
                    note: _note,
                    exchangeType: state.exchangeType,
                    categoryId: state.categoryId,
                    amount: _amount,
                    date: state.date,
                    image: image ?? state.image),
                isAdding: false),
          );
        } else {
          BlocProvider.of<UpdateExchangeBloc>(context).add(SubmitedChanges(
            exchange: Exchange(
              name: _name,
              note: _note,
              exchangeType: state.exchangeType,
              categoryId: state.categoryId,
              amount: _amount,
              date: state.date,
              image: image ?? state.image,
            ),
            isAdding: true,
          ));
        }
        Navigator.pop(context);
        return 0;
      } catch (e) {
        print(e.toString());
        Navigator.pop(context);
      }
    }
    return -1;
  }

  Future<String> uploadImage(String filePath) async {
    try {
      final fileName = path.basename(filePath);
      StorageReference firebaseStorageRef =
          FirebaseStorage.instance.ref().child(fileName);
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(File(filePath));
      var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
      return dowurl.toString();
    } catch (e) {
      return null;
    }
  }
}
