import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:app_thu_chi/screens/add_edit_screen.dart/add_edit_screen.dart';
import 'widgets/widget_home.dart';

class HomeScreen extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(
      builder: (context, activeTab) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: activeTab == AppTab.stats
                ? Text(
                    BlocProvider.of<LocalizationBloc>(context).state.statsTitle)
                : Text(BlocProvider.of<LocalizationBloc>(context)
                    .state
                    .exchangesTitle),
            actions: <Widget>[
              LanguageButton(),
            ],
          ),
          //drawer: Drawer(),
          body: activeTab == AppTab.exchanges ? ExchangesTab() : Stats(),
          floatingActionButton: activeTab == AppTab.stats
              ? Container()
              : FloatingActionButton(
                  tooltip: BlocProvider.of<LocalizationBloc>(context)
                      .state
                      .addFabTooltip,
                  onPressed: () async {
                    final saved = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                AddEditScreen(isEditing: false)));
                    if (saved != null) {
                      Helper()
                          .displaySaveSuccessSnackbar(context, _scaffoldKey);
                    }
                  },
                  child: Icon(Icons.add),
                ),
          bottomNavigationBar: TabSelector(
            tabSelected: activeTab,
            onTabSelected: (selectedTab) {
              BlocProvider.of<TabBloc>(context)
                  .add(TabChanged(appTab: selectedTab));
            },
          ),
        );
      },
    );
  }
}
