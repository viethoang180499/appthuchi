import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/models/exchange.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DeleteExchangeSnackBar extends SnackBar {
  DeleteExchangeSnackBar(
      {Exchange exchange, VoidCallback onUndo, BuildContext context})
      : super(
          backgroundColor: Colors.red,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon(Icons.delete),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    BlocProvider.of<LocalizationBloc>(context)
                        .state
                        .notiDelSuccess,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label: BlocProvider.of<LocalizationBloc>(context)
                .state
                .labelDelExchangeSnackBar,
            onPressed: onUndo,
            textColor: Colors.white,
          ),
        );
}
