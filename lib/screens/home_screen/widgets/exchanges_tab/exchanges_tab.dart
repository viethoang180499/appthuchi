import 'dart:async';

import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/screens/detail_screen/detailScreen.dart';
import 'package:app_thu_chi/screens/home_screen/widgets/exchanges_tab/filter_button.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'delete_exchange_snackbar.dart';
import 'exchange_item.dart';

class ExchangesTab extends StatefulWidget {
  @override
  _ExchangesTabState createState() => _ExchangesTabState();
}

class _ExchangesTabState extends State<ExchangesTab> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    super.initState();
  }

  void _onScroll() {
    //final maxScroll = _scrollController.position.maxScrollExtent;
    //final currentScroll = _scrollController.position.pixels;
    // if (maxScroll == currentScroll) {
    //   BlocProvider.of<FilteredExchangesBloc>(context).add(Fetch());
    // }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilteredExchangesBloc, FilteredExchangesState>(
      builder: (context, state) {
        if (state is FilteredExchangesLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is FilteredExchangesLoaded) {
          final exchanges = state.exchanges;
          return Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          hintText: BlocProvider.of<LocalizationBloc>(context)
                              .state
                              .filterBarHintText),
                      onChanged: (text) {
                        final _debouncer = Debouncer(milliseconds: 50);
                        _debouncer.run(() =>
                            BlocProvider.of<FilteredExchangesBloc>(context)
                                .add(SearchExchanges(name: text)));
                      },
                    ),
                  ),
                  FilterButton(),
                ],
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: exchanges.length,
                  itemBuilder: (context, index) {
                    final exchange = exchanges[index];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        (index > 0 &&
                                    !Helper().isSameDay(
                                        exchanges[index - 1].date,
                                        exchange.date)) ||
                                index == 0
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15, 15, 0, 0),
                                child: Text(formatDate(
                                    exchange.date, [dd, '/', mm, '/', yyyy])),
                              )
                            : Container(),
                        ExchangeItem(
                          exchange: exchange,
                          categoryId: exchange.categoryId,
                          onTap: () async {
                            final removedExchange = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    DetailScreen(exchangeId: exchange.id),
                              ),
                            );
                            if (removedExchange != null) {
                              Scaffold.of(context).showSnackBar(
                                DeleteExchangeSnackBar(
                                  context: context,
                                  exchange: exchange,
                                  onUndo: () =>
                                      BlocProvider.of<ExchangesBloc>(context)
                                          .add(
                                    ExchangeAdded(exchange: exchange),
                                  ),
                                ),
                              );
                            }
                          },
                        )
                      ],
                    );
                  },
                ),
              )
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}

// class Exchanges extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<FilteredExchangesBloc, FilteredExchangesState>(
//       builder: (context, state) {
//         if (state is FilteredExchangesLoading) {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         } else if (state is FilteredExchangesLoaded) {
//           final exchanges = state.exchanges;
//           return Column(
//             children: <Widget>[
//               TextField(
//                 decoration: InputDecoration(
//                     contentPadding: EdgeInsets.all(10),
//                     hintText: 'Filter by name'),
//                 onChanged: (text) {
//                   final _debouncer = Debouncer(milliseconds: 50);
//                   _debouncer.run(() =>
//                       BlocProvider.of<FilteredExchangesBloc>(context)
//                           .add(SearchExchanges(name: text)));
//                 },
//               ),
//               Expanded(
//                 child: ListView.builder(
//                   itemCount: exchanges.length,
//                   itemBuilder: (context, index) {
//                     final exchange = exchanges[index];
//                     return Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         (index > 0 &&
//                                     !Helper().isSameDay(
//                                         exchanges[index - 1].date,
//                                         exchange.date)) ||
//                                 index == 0
//                             ? Padding(
//                                 padding:
//                                     const EdgeInsets.fromLTRB(15, 15, 0, 0),
//                                 child: Text(formatDate(
//                                     exchange.date, [dd, '/', mm, '/', yyyy])),
//                               )
//                             : Container(),
//                         ExchangeItem(
//                           exchange: exchange,
//                           categoryId: exchange.categoryId,
//                           onTap: () async {
//                             final removedExchange = await Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                 builder: (context) =>
//                                     DetailScreen(exchangeId: exchange.id),
//                               ),
//                             );
//                             if (removedExchange != null) {
//                               Scaffold.of(context).showSnackBar(
//                                 DeleteExchangeSnackBar(
//                                   exchange: exchange,
//                                   onUndo: () =>
//                                       BlocProvider.of<ExchangesBloc>(context)
//                                           .add(
//                                     ExchangeAdded(exchange: exchange),
//                                   ),
//                                 ),
//                               );
//                             }
//                             print('onTapExchangeItem');
//                           },
//                         )
//                       ],
//                     );
//                   },
//                 ),
//               )
//             ],
//           );
//         } else {
//           return Container();
//         }
//       },
//     );
//   }
// }

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
