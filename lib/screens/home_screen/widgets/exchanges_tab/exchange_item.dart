import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class ExchangeItem extends StatelessWidget {
  final Exchange exchange;
  final Function() onTap;
  final String categoryId;

  const ExchangeItem(
      {Key key,
      @required this.exchange,
      @required this.onTap,
      @required this.categoryId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategorysBloc, CategorysState>(
        builder: (context, state) {
      var cate;
      if (state is CategorysLoadSuccess) {
        cate = state.categorys.firstWhere(
            (catefory) => catefory.id == categoryId,
            orElse: () => null);
      }
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              child: ListTile(
                title: Text(
                  exchange.name,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                subtitle: Text(
                  cate == null ? '' : cate.name,
                ),
                leading: Icon(Icons.cached),
                trailing: exchange.exchangeType == ExchangeType.income
                    ? Text(
                        '+${exchange.amount}đ',
                        style: TextStyle(color: Colors.green),
                      )
                    : Text(
                        '-${exchange.amount}đ',
                        style: TextStyle(color: Colors.red),
                      ),
                onTap: onTap,
              ),
            )
          ],
        ),
      );
    });
  }
}
