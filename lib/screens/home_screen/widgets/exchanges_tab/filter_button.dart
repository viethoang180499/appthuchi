import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FilterButton extends StatelessWidget {

  // const FilterButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilteredExchangesBloc, FilteredExchangesState>(
      builder: (context, state) {
        return _Button(
          onSelected: (exchangeType) =>
              BlocProvider.of<FilteredExchangesBloc>(context).add(
            FilterUpdate(
              exchangeType: exchangeType,
            ),
          ),
          exchangeType: state is FilteredExchangesLoaded
              ? state.exchangeType
              : ExchangeType.all,
        );
        // return AnimatedOpacity(
        //   opacity: visible ? 1.0 : 0.0,
        //   duration: Duration(microseconds: 150),
        //   child: visible
        //       ? button
        //       : IgnorePointer(
        //           child: button,
        //         ),
        // );
      },
    );
  }
}

class _Button extends StatelessWidget {
  final Function(ExchangeType) onSelected;
  final ExchangeType exchangeType;

  const _Button(
      {Key key, @required this.onSelected, @required this.exchangeType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<ExchangeType>(
      //elevation: double.infinity,
      itemBuilder: (context) => <PopupMenuItem<ExchangeType>>[
        PopupMenuItem(
          child: Text(
            BlocProvider.of<LocalizationBloc>(context).state.incomes,
            style: TextStyle(
              color: exchangeType == ExchangeType.income
                  ? Colors.blue[800]
                  : Colors.black,
            ),
          ),
          value: ExchangeType.income,
        ),
        PopupMenuItem(
          child: Text(
            BlocProvider.of<LocalizationBloc>(context).state.expenses,
            style: TextStyle(
              color: exchangeType == ExchangeType.expense
                  ? Colors.blue[800]
                  : Colors.black,
            ),
          ),
          value: ExchangeType.expense,
        ),
        PopupMenuItem(
          child: Text(
            BlocProvider.of<LocalizationBloc>(context).state.all,
            style: TextStyle(
              color: exchangeType == ExchangeType.all
                  ? Colors.blue[800]
                  : Colors.black,
            ),
          ),
          value: ExchangeType.all,
        ),
      ],
      icon: Icon(Icons.filter_list),
      onSelected: onSelected,
    );
  }
}
