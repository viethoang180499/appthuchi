import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LanguageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      padding: EdgeInsets.only(right: 10),
      itemBuilder: (context) => <PopupMenuItem>[
        PopupMenuItem(
          child: Row(
            children: <Widget>[
              Image.asset(
                'assets/vietnamese.jpg',
                width: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(BlocProvider.of<LocalizationBloc>(context)
                    .state
                    .vietnamese),
              ),
            ],
          ),
          value: Locale('vi', ''),
        ),
        PopupMenuItem(
          child: Row(
            children: <Widget>[
              Image.asset(
                'assets/english.png',
                width: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                    BlocProvider.of<LocalizationBloc>(context).state.english),
              ),
            ],
          ),
          value: Locale('en', ''),
        ),
      ],
      icon: Icon(Icons.language),
      onSelected: (value) {
        BlocProvider.of<LocalizationBloc>(context)
            .add(LocalizationChanged(locale: value));
      },
    );
  }
}
