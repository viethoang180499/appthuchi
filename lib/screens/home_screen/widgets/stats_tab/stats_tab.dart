import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'overrall_chart.dart';
import 'specific_chart.dart';

class Stats extends StatefulWidget {
  @override
  _StatsState createState() => _StatsState();
}

class _StatsState extends State<Stats> {
  DateTime _startDay;
  DateTime _endDay;

  @override
  void initState() {
    final now = DateTime.now();
    _startDay = DateTime(now.year, now.month, 1);
    _endDay = DateTime.now();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.calendar_today),
                      //SizedBox(width: 2),
                      Text(
                        BlocProvider.of<LocalizationBloc>(context).state.from +
                            formatDate(_startDay, [dd, '/', mm, '/', yyyy]),
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ],
                  ),
                  onTap: () async {
                    _startDay = await showDatePicker(
                            context: context,
                            initialDate: _startDay,
                            firstDate: DateTime(2015, 8),
                            lastDate: DateTime(2101)) ??
                        _startDay;
                    setState(() {});
                  },
                ),
                GestureDetector(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.calendar_today),
                      //SizedBox(width: 5),
                      Text(
                        BlocProvider.of<LocalizationBloc>(context).state.to +
                            formatDate(_endDay, [dd, '/', mm, '/', yyyy]),
                        style: TextStyle(
                          //color: Colors.blue,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ],
                  ),
                  onTap: () async {
                    _endDay = await showDatePicker(
                            context: context,
                            initialDate: _endDay,
                            firstDate: DateTime(2015, 8),
                            lastDate: DateTime(2101)) ??
                        _endDay;
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
          Container(
            color: Colors.grey[200],
            child: Column(
              children: <Widget>[
                Card(
                  child: OverrallChart(
                    startDay: _startDay,
                    endDay: _endDay,
                  ),
                ),
                Card(
                  child: SpecificChart(
                    startDay: _startDay,
                    endDay: _endDay,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
