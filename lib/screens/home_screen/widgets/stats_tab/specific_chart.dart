import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/models/category.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_bloc/flutter_bloc.dart';


class SpecificChart extends StatelessWidget {
  final DateTime startDay;
  final DateTime endDay;

  const SpecificChart({Key key, @required this.startDay, @required this.endDay})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              BlocProvider.of<LocalizationBloc>(context).state.specificChartName,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 210,
            width: double.infinity,
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Text(
                          BlocProvider.of<LocalizationBloc>(context).state.incomes,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.green,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(top: 5),
                          width: 170,
                          child: charts.PieChart(
                            _createData(context, ExchangeType.income),
                            animate: true,
                            animationDuration: Duration(seconds: 1),
                            defaultRenderer: new charts.ArcRendererConfig(
                              arcRendererDecorators: [
                                new charts.ArcLabelDecorator(
                                    labelPosition: charts.ArcLabelPosition.auto)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Text(
                          BlocProvider.of<LocalizationBloc>(context).state.expenses,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.red,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: 170,
                          child: charts.PieChart(
                            _createData(context, ExchangeType.expense),
                            animate: true,
                            animationDuration: Duration(seconds: 1),
                            defaultRenderer: new charts.ArcRendererConfig(
                              arcRendererDecorators: [
                                new charts.ArcLabelDecorator(
                                    labelPosition: charts.ArcLabelPosition.auto)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IntrinsicWidth(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: _buildAnnonates(context),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  List<charts.Series<CategorysSummary, String>> _createData(
      BuildContext context, ExchangeType exchangeType) {
    List<CategorysSummary> data = [];
    final categorys =
        (BlocProvider.of<CategorysBloc>(context).state as CategorysLoadSuccess)
            .categorys;
    final List<Color> colors = [];
    for (int i = 0; i < categorys.length; i++) {
      colors.add(Colors.primaries[i]);
    }
    int j = 0;
    for (var category in categorys) {
      int categorysSummary = Helper().exchangesCategorySummary(context,
          exchangeType: exchangeType,
          categoryId: category.id,
          startDay: startDay,
          endDay: endDay);
      data.add(
        new CategorysSummary(
          exchangeType,
          category,
          categorysSummary,
          colors[j++],
        ),
      );
    }

    return [
      new charts.Series<CategorysSummary, String>(
        id: 'categorys',
        domainFn: (CategorysSummary category, _) => category.category.id,
        measureFn: (CategorysSummary category, _) => category.summary,
        colorFn: (CategorysSummary category, _) =>
            charts.ColorUtil.fromDartColor(category.color),
        data: data,
        labelAccessorFn: (CategorysSummary row, _) =>
            row.summary == 0 ? '' : '${row.summary}',
      )
    ];
  }

  List<Widget> _buildAnnonates(BuildContext context) {
    final categorys =
        (BlocProvider.of<CategorysBloc>(context).state as CategorysLoadSuccess)
            .categorys;
    final List<Color> colors = [];
    for (int i = 0; i < categorys.length; i++) {
      colors.add(Colors.primaries[i]);
    }
    return List.generate(
      categorys.length,
      (index) => Container(
        padding: EdgeInsets.only(bottom: 5),
        child: Helper().exchangesCategorySummary(context,
                    exchangeType: ExchangeType.all,
                    categoryId: categorys[index].id,
                    startDay: startDay,
                    endDay: endDay) ==
                0
            ? Container()
            : Row(
                children: <Widget>[
                  Container(
                    width: 20,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: colors[index],
                    ),
                    child: Text(''),
                  ),
                  Text(' ${categorys[index].name}'),
                ],
              ),
      ),
    );
  }
}

class CategorysSummary {
  final ExchangeType exchangesType;
  final Category category;
  final int summary;
  final Color color;

  CategorysSummary(this.exchangesType, this.category, this.summary, this.color);
}
