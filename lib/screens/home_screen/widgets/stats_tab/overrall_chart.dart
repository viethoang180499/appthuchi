import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:app_thu_chi/utils/helper.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_bloc/flutter_bloc.dart';

class OverrallChart extends StatelessWidget {
  final DateTime startDay;
  final DateTime endDay;

  const OverrallChart({Key key, @required this.startDay, @required this.endDay})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int expensesSummary = Helper().exchangeSummary(
      context,
      exchangeType: ExchangeType.expense,
      startDay: startDay,
      endDay: endDay,
    );

    int incomeSummary = Helper().exchangeSummary(
      context,
      exchangeType: ExchangeType.income,
      startDay: startDay,
      endDay: endDay,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            BlocProvider.of<LocalizationBloc>(context).state.overrallChartName,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 210),
          child: Row(
            children: <Widget>[
              Text(BlocProvider.of<LocalizationBloc>(context).state.saving),
              expensesSummary - incomeSummary > 0
                  ? Text(
                      '-${expensesSummary - incomeSummary}${BlocProvider.of<LocalizationBloc>(context).state.currency}',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    )
                  : Text(
                      '+${incomeSummary - expensesSummary}${BlocProvider.of<LocalizationBloc>(context).state.currency}',
                      style: TextStyle(
                        color: Colors.green,
                      ),
                    ),
            ],
          ),
        ),
        Container(
          height: 200,
          child: charts.PieChart(
            _createData(context, expensesSummary, incomeSummary),
            animate: true,
            animationDuration: Duration(seconds: 1),
            defaultRenderer: new charts.ArcRendererConfig(
              arcRendererDecorators: [
                new charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.outside)
              ],
            ),
          ),
        ),
      ],
    );
  }

  List<charts.Series<ExchangesSummary, int>> _createData(
      BuildContext context, int expensesSummary, int incomeSummary) {
    final data = [
      new ExchangesSummary(ExchangeType.income, incomeSummary),
      new ExchangesSummary(ExchangeType.expense, expensesSummary),
    ];

    return [
      new charts.Series<ExchangesSummary, int>(
        id: 'exchanges',
        domainFn: (ExchangesSummary exchanges, _) =>
            exchanges.exchangesType.index,
        measureFn: (ExchangesSummary exchanges, _) => exchanges.summary,
        // colorFn: (ExchangesSummary exchanges, _) =>
        //     charts.ColorUtil.fromDartColor(
        //         exchanges.exchangesType == ExchangeType.income
        //             ? Colors.green
        //             : Colors.red),
        data: data,
        labelAccessorFn: (ExchangesSummary row, _) =>
            row.exchangesType == ExchangeType.income
                ? '${BlocProvider.of<LocalizationBloc>(context).state.incomes}: ${row.summary.toString()}'
                : '${BlocProvider.of<LocalizationBloc>(context).state.expenses}: ${row.summary.toString()}',
      )
    ];
  }
}

class ExchangesSummary {
  final ExchangeType exchangesType;
  final int summary;

  ExchangesSummary(this.exchangesType, this.summary);
}
