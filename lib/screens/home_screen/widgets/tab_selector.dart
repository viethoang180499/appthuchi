import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TabSelector extends StatelessWidget {
  final AppTab tabSelected;
  final Function(AppTab) onTabSelected;

  const TabSelector(
      {Key key, @required this.tabSelected, @required this.onTabSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: AppTab.values
          .map(
            (tab) => BottomNavigationBarItem(
              icon: tab == AppTab.exchanges
                  ? Icon(Icons.attach_money)
                  : Icon(Icons.show_chart),
              title: Text(tab == AppTab.exchanges
                  ? BlocProvider.of<LocalizationBloc>(context)
                      .state
                      .tabExchangesName
                  : BlocProvider.of<LocalizationBloc>(context)
                      .state
                      .tabStatsName),
            ),
          )
          .toList(),
      currentIndex: tabSelected.index,
      onTap: (index) => onTabSelected(AppTab.values[index]),
    );
  }
}
