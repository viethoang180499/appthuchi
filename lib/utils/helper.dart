import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'enums.dart';


class Helper {
  String datetimeToDay(DateTime date) {
    return formatDate(date, [DD, ' ', dd, '/', mm, '/', yyyy]);
  }

  String datetimeToTime(DateTime date) {
    return formatDate(date, [hh, ':', nn, ' ', am]);
  }

  void displaySaveSuccessSnackbar(
      BuildContext context, GlobalKey<ScaffoldState> _scaffoldKey) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        duration: Duration(milliseconds: 500),
        content: Row(
          children: <Widget>[
            Icon(Icons.done),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text('Save successfully'),
            ),
          ],
        ),
        backgroundColor: Colors.blue,
      ),
    );
  }

  int exchangeSummary(BuildContext context,
      {ExchangeType exchangeType, DateTime startDay, DateTime endDay}) {
    int result = 0;
    final exchanges =
        (BlocProvider.of<ExchangesBloc>(context).state as ExchangesLoadSuccess)
            .exchanges
            .where((exchange) =>
                exchange.exchangeType == exchangeType &&
                startDay.compareTo(exchange.date) != 1 &&
                endDay.compareTo(exchange.date) != -1)
            .toList();
    for (var exchange in exchanges) {
      if (exchange.amount != null) {
        result += exchange.amount;
      }
    }
    return result;
  }

  int exchangesCategorySummary(BuildContext context,
      {ExchangeType exchangeType,
      String categoryId,
      DateTime startDay,
      DateTime endDay}) {
    int result = 0;
    final exchanges =
        (BlocProvider.of<ExchangesBloc>(context).state as ExchangesLoadSuccess)
            .exchanges
            .where((exchange) =>
                (exchangeType == ExchangeType.all ||
                    exchange.exchangeType == exchangeType) &&
                exchange.categoryId == categoryId &&
                startDay.compareTo(exchange.date) != 1 &&
                endDay.compareTo(exchange.date) != -1)
            .toList();
    for (var exchange in exchanges) {
      if (exchange.amount != null) {
        result += exchange.amount;
      }
    }
    return result;
  }

  bool isSameDay(DateTime d1, DateTime d2) {
    return (d1.day == d2.day && d1.month == d2.month && d1.year == d2.year);
  }
}
