enum ExchangeType { income, expense, all }

enum AppTab { stats, exchanges }

enum TypeSort { increase, decrease, none }

enum SortBy { name, date }
