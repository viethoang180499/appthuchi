import 'package:app_thu_chi/screens/add_edit_screen.dart/add_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'blocs/bloc.dart';
import 'screens/home_screen/home_screen.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  '/': (BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => TabBloc()),
          BlocProvider(
            create: (context) => FilteredExchangesBloc(
              exchangesBloc: BlocProvider.of<ExchangesBloc>(context),
            ),
          ),
        ],
        child: HomeScreen(),
      ),
  AddEditScreen.routeName: (BuildContext context) => AddEditScreen(
        isEditing: false,
      ),
};
