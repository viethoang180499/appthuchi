import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/blocs/simple_bloc_delegate.dart';
import 'package:app_thu_chi/repository/exchangeRepo.dart';
import 'package:app_thu_chi/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'repository/categoryRepo.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ExchangesBloc(exchangeRepo: ExchangeRepository())
            ..add(ExchangesLoad()),
        ),
        BlocProvider(
            create: (context) =>
                CategorysBloc(categoryRepo: CategoryRepository())
                  ..add(CategorysLoad())),
        BlocProvider(create: (context) => LocalizationBloc()),
      ],
      child: BlocBuilder<LocalizationBloc, Localization>(
        builder: (BuildContext context, Localization state) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Bugget Management App',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          localizationsDelegates: [
            const LocalizationBlocDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en', ''),
            const Locale('vi', ''),
          ],
          initialRoute: '/',
          routes: routes,
        ),
      ),
    );
  }
}
