import 'package:app_thu_chi/models/exchange.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ExchangeRepository {
  final exchangeColecction = Firestore.instance.collection('exchanges');

  Stream<List<Exchange>> exchanges() {
    return exchangeColecction.snapshots().map((snapshot) {
      return snapshot.documents
          .map((doc) => Exchange.fromSnapshot(doc))
          .toList();
    });
  }

  Future<void> addNewExchange(Exchange exchange) async {
    return await exchangeColecction.add(exchange.toDocument());
  }

  Future<void> updateExchange(Exchange exchange) async {
    return await exchangeColecction
        .document(exchange.id)
        .updateData(exchange.toDocument());
  }

  Future<void> deleteExchange(Exchange exchange) async {
    return await exchangeColecction.document(exchange.id).delete();
  }
}
