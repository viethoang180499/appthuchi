import 'package:app_thu_chi/models/category.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryRepository {
  final categoryCollection = Firestore.instance.collection('categorys');

  Stream<List<Category>> categorys() {
    return categoryCollection.snapshots().map((snapshot) {
      return snapshot.documents
          .map((doc) => Category.fromSnapshot(doc))
          .toList();
    });
  }

  Future<void> addNewCategory(Category category) async {
    return await categoryCollection.add(category.toDocument());
  }

  Future<void> updateCategory(Category category) async {
    return await categoryCollection
        .document(category.id)
        .updateData(category.toDocument());
  }

  Future<void> deleteCategory(Category category) async {
    return await categoryCollection.document(category.id).delete();
  }
}
