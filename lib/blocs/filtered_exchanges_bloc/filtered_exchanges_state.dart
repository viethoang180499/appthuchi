import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:equatable/equatable.dart';

abstract class FilteredExchangesState extends Equatable {
  const FilteredExchangesState();
  @override
  List<Object> get props => [];
}

class FilteredExchangesLoading extends FilteredExchangesState {}

class FilteredExchangesLoaded extends FilteredExchangesState {
  final List<Exchange> exchanges;
  final ExchangeType exchangeType;
  final TypeSort typeSort;
  final SortBy sortBy;
  final String name;
  final bool hasReachedMax;

  const FilteredExchangesLoaded(
      {this.exchanges,
      this.hasReachedMax,
      this.exchangeType,
      this.typeSort,
      this.sortBy,
      this.name});

  @override
  List<Object> get props => [exchanges, exchangeType, typeSort, sortBy, name];

  @override
  String toString() =>
      'FilteredExchanges Loaded: exchanges: $exchanges, type: $exchangeType, typeSort: $typeSort, sortBy: $sortBy, name: $name';
}
