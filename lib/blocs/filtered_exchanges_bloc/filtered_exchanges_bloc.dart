import 'dart:async';
import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/blocs/filtered_exchanges_bloc/filtered_exchanges.dart';
import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FilteredExchangesBloc
    extends Bloc<FilteredExchangesEvent, FilteredExchangesState> {
  StreamSubscription exchangesSubs;
  final ExchangesBloc exchangesBloc;

  FilteredExchangesBloc({@required this.exchangesBloc}) {
    exchangesSubs = exchangesBloc.listen((state) {
      if (state is ExchangesLoadSuccess) {
        add(ExchangesUpdate(exchanges: state.exchanges));
      }
    });
  }

  @override
  FilteredExchangesState get initialState {
    return exchangesBloc.state is ExchangesLoadSuccess
        ? FilteredExchangesLoaded(
            exchanges: (exchangesBloc.state as ExchangesLoadSuccess).exchanges)
        : FilteredExchangesLoading();
  }

  @override
  Stream<FilteredExchangesState> mapEventToState(
      FilteredExchangesEvent event) async* {
    if (event is FilterUpdate) {
      yield* _mapFilterUpdateToState(event);
    } else if (event is ExchangesUpdate) {
      yield* _mapExchangesUpdateToState(event);
    } else if (event is SearchExchanges) {
      yield* _mapSearchExchangesToState(event);
    } else if (event is Fetch) {
      yield* _mapFetchToState();
    }
  }

  Stream<FilteredExchangesState> _mapFetchToState() async* {}

  // bool _hasReachedMax(FilteredExchangesState state) =>
  //     state is FilteredExchangesLoaded && state.hasReachedMax;

  //Future<List<Exchange>> _fetchPost({int startIndex, int limit}) async {}

  Stream<FilteredExchangesState> _mapExchangesUpdateToState(
      ExchangesUpdate event) async* {
    yield FilteredExchangesLoaded(
      exchanges: _mapExchangesToFilterExchanges(
          (exchangesBloc.state as ExchangesLoadSuccess).exchanges,
          exchangeType: (state is FilteredExchangesLoaded)
              ? (state as FilteredExchangesLoaded).exchangeType
              : ExchangeType.all,
          // typeSort: event.typeSort,
          // sortBy: event.sortBy,
          name: state is FilteredExchangesLoaded
              ? (state as FilteredExchangesLoaded).name
              : ''),
      exchangeType: state is FilteredExchangesLoaded
          ? (state as FilteredExchangesLoaded).exchangeType
          : ExchangeType.all,
      // typeSort: event.typeSort,
      // sortBy: event.sortBy,
      name: state is FilteredExchangesLoaded
          ? (state as FilteredExchangesLoaded).name
          : '',
    );
  }

  Stream<FilteredExchangesState> _mapFilterUpdateToState(
      FilterUpdate event) async* {
    if (exchangesBloc.state is ExchangesLoadSuccess) {
      yield FilteredExchangesLoaded(
        exchanges: _mapExchangesToFilterExchanges(
          (exchangesBloc.state as ExchangesLoadSuccess).exchanges,
          exchangeType: event.exchangeType,
          typeSort: event.typeSort,
          sortBy: event.sortBy,
          name: (state as FilteredExchangesLoaded).name,
        ),
        exchangeType: event.exchangeType,
        // typeSort: event.typeSort,
        // sortBy: event.sortBy,
        name: (state as FilteredExchangesLoaded).name,
      );
    }
  }

  Stream<FilteredExchangesState> _mapSearchExchangesToState(
      SearchExchanges event) async* {
    if (exchangesBloc.state is ExchangesLoadSuccess) {
      yield FilteredExchangesLoaded(
        exchanges: _mapExchangesToFilterExchanges(
          (exchangesBloc.state as ExchangesLoadSuccess).exchanges,
          exchangeType: (state as FilteredExchangesLoaded).exchangeType,
          // typeSort: event.typeSort,
          // sortBy: event.sortBy,
          name: event.name,
        ),
        exchangeType: (state as FilteredExchangesLoaded).exchangeType,
        // typeSort: (state as FilteredExchangesLoaded).typeSort,
        // sortBy: (state as FilteredExchangesLoaded).sortBy,
        name: event.name,
      );
    }
  }

  List<Exchange> _mapExchangesToFilterExchanges(List<Exchange> exchanges,
      {ExchangeType exchangeType = ExchangeType.all,
      TypeSort typeSort = TypeSort.none,
      SortBy sortBy = SortBy.name,
      String name = ''}) {
    final results = exchanges
        .where((exchange) => exchangeType == ExchangeType.all
            ? true
            : exchange.exchangeType == exchangeType)
        .toList();
    if (typeSort != TypeSort.none) {
      results.sort((a, b) {
        if (typeSort == TypeSort.increase) {
          return sortBy == SortBy.name
              ? a.name.compareTo(b.name)
              : a.date.compareTo(b.date);
        } else {
          return sortBy == SortBy.name
              ? b.name.compareTo(a.name)
              : b.date.compareTo(a.date);
        }
      });
    } else {
      results.sort((a, b) => b.date.compareTo(a.date));
    }

    if (name != null && name != '') {
      return results
          .where((exchange) =>
              exchange.name.toLowerCase().contains(name.toLowerCase()))
          .toList();
    }

    return results;
  }

  @override
  Future<void> close() {
    exchangesSubs?.cancel();
    return super.close();
  }
}
