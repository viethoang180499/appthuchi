import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class FilteredExchangesEvent extends Equatable {
  const FilteredExchangesEvent();
  @override
  List<Object> get props => [];
}

class FilterUpdate extends FilteredExchangesEvent {
  final ExchangeType exchangeType;
  final TypeSort typeSort;
  final SortBy sortBy;
  final String name;

  const FilterUpdate(
      {ExchangeType exchangeType,
      TypeSort typeSort,
      SortBy sortBy,
      String name})
      : exchangeType = exchangeType ?? ExchangeType.all,
        typeSort = typeSort ?? TypeSort.none,
        sortBy = sortBy ?? null,
        name = name ?? '';

  @override
  List<Object> get props => [exchangeType, typeSort, sortBy];

  @override
  String toString() =>
      'FilterUpdate: exchangeType: $exchangeType, typeSort: $typeSort, sortBy: $sortBy, name: $name';
}

class SearchExchanges extends FilteredExchangesEvent {
  final String name;
  const SearchExchanges({@required this.name});

  @override
  List<Object> get props => [name];

  @override
  String toString() => 'SearchExchanges: $name';
}

class ExchangesUpdate extends FilteredExchangesEvent {
  final List<Exchange> exchanges;
  const ExchangesUpdate({@required this.exchanges});

  @override
  List<Object> get props => [exchanges];

  @override
  String toString() => 'ExchangesUpdate: $exchanges';
}

class Fetch extends FilteredExchangesEvent {}

class FiltedError extends FilteredExchangesState {}
