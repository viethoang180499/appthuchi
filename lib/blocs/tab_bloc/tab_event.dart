import 'package:app_thu_chi/utils/enums.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class TabEvent extends Equatable {
  const TabEvent();
}

class TabChanged extends TabEvent {
  final AppTab appTab;

  const TabChanged({@required this.appTab}) : assert(appTab != null);

  @override
  List<Object> get props => [appTab];

  @override
  String toString() => 'TabChanged: $appTab';
}
