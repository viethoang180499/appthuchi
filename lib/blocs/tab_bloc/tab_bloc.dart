import 'dart:async';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:bloc/bloc.dart';
import 'tab.dart';

class TabBloc extends Bloc<TabEvent, AppTab> {
  @override
  AppTab get initialState => AppTab.exchanges;

  @override
  Stream<AppTab> mapEventToState(TabEvent event) async* {
    if (event is TabChanged) {
      yield event.appTab;
    }
  }
}
