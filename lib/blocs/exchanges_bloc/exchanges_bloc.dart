import 'dart:async';
import 'package:app_thu_chi/repository/exchangeRepo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'exchanges.dart';

class ExchangesBloc extends Bloc<ExchangesEvent, ExchangesState> {
  final ExchangeRepository exchangeRepo;
  StreamSubscription _exchangeSubcription;

  ExchangesBloc({@required this.exchangeRepo}) : assert(exchangeRepo != null);

  @override
  ExchangesState get initialState => ExchangesLoading();

  @override
  Stream<ExchangesState> mapEventToState(ExchangesEvent event) async* {
    if (event is ExchangesLoad) {
      yield* _mapExchangesLoadToState(event);
    } else if (event is ExchangeAdded) {
      yield* _mapExchangeAddedToState(event);
    } else if (event is ExchangeUpdated) {
      yield* _mapExchangeUpdatedToState(event);
    } else if (event is ExchangeDeleted) {
      yield* _mapExchangeDeletedToState(event);
    } else if (event is ExchangesUpdated) {
      yield* _mapExchangesUpdateToState(event);
    }
  }

  Stream<ExchangesState> _mapExchangesLoadToState(ExchangesLoad event) async* {
    _exchangeSubcription?.cancel();
    _exchangeSubcription = exchangeRepo.exchanges().listen(
          (exchanges) => add(
            ExchangesUpdated(exchanges: exchanges),
          ),
        );
  }

  Stream<ExchangesState> _mapExchangeAddedToState(ExchangeAdded event) async* {
    await exchangeRepo.addNewExchange(event.exchange);
  }

  Stream<ExchangesState> _mapExchangeUpdatedToState(
      ExchangeUpdated event) async* {
    await exchangeRepo.updateExchange(event.exchange);
  }

  Stream<ExchangesState> _mapExchangeDeletedToState(
      ExchangeDeleted event) async* {
    await exchangeRepo.deleteExchange(event.exchange);
  }

  Stream<ExchangesState> _mapExchangesUpdateToState(
      ExchangesUpdated event) async* {
    yield ExchangesLoadSuccess(exchanges: event.exchanges);
  }

  @override
  Future<void> close() {
    _exchangeSubcription?.cancel();
    return super.close();
  }
}
