import 'package:app_thu_chi/models/exchange.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ExchangesState extends Equatable {
  const ExchangesState();
  @override
  List<Object> get props => [];
}

class ExchangesLoading extends ExchangesState {}

class ExchangesLoadSuccess extends ExchangesState {
  final List<Exchange> exchanges;

  const ExchangesLoadSuccess({@required this.exchanges});

  @override
  List<Object> get props => [exchanges];

  @override
  String toString() => 'ExchangesLoadSuccess: ';
}

class ExchangesLoadFailure extends ExchangesState {}
