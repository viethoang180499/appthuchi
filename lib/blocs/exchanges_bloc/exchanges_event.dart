import 'package:app_thu_chi/models/exchange.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ExchangesEvent extends Equatable {
  const ExchangesEvent();
  @override
  List<Object> get props => [];
}

class ExchangesLoad extends ExchangesEvent {}

class ExchangeAdded extends ExchangesEvent {
  final Exchange exchange;

  const ExchangeAdded({@required this.exchange});

  @override
  List<Object> get props => [exchange];

  @override
  String toString() => 'ExchangesAdded: ${exchange.name}';
}

class ExchangeUpdated extends ExchangesEvent {
  final Exchange exchange;

  const ExchangeUpdated({@required this.exchange});

  @override
  List<Object> get props => [exchange];

  @override
  String toString() => 'ExchangesUpdated: ${exchange.name}';
}

class ExchangeDeleted extends ExchangesEvent {
  final Exchange exchange;

  const ExchangeDeleted({@required this.exchange});

  @override
  List<Object> get props => [exchange];

  @override
  String toString() => 'ExchangesDeleted: ${exchange.name}';
}

class ExchangesUpdated extends ExchangesEvent {
  final List<Exchange> exchanges;

  const ExchangesUpdated({@required this.exchanges});

  @override
  List<Object> get props => [exchanges];
}
