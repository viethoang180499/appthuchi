import 'package:app_thu_chi/models/category.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class CategorysState extends Equatable {
  const CategorysState();
  @override
  List<Object> get props => [];
}

class CategorysLoading extends CategorysState{}

class CategorysLoadSuccess extends CategorysState {
  final List<Category> categorys;

  const CategorysLoadSuccess({@required this.categorys});

  @override
  List<Object> get props => [categorys];

  @override
  String toString() => 'CategorysLoadSuccess: $categorys';
}

class CategorysLoadFailure extends CategorysState {}
