import 'package:app_thu_chi/models/category.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class CategorysEvent extends Equatable {
  const CategorysEvent();
  @override
  List<Object> get props => [];
}

class CategorysLoad extends CategorysEvent {}

class CategoryAdded extends CategorysEvent {
  final Category category;

  const CategoryAdded({@required this.category});

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'CategoryAdded: $category';
}

class CategoryUpdated extends CategorysEvent {
  final Category category;

  const CategoryUpdated({@required this.category});

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'CategoryUpdated: $category';
}

class CategoryDeleted extends CategorysEvent {
  final Category category;

  const CategoryDeleted({@required this.category});

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'CategoryDeleted: $category';
}

class CategorysUpdate extends CategorysEvent {
  final List<Category> categorys;

  const CategorysUpdate({@required this.categorys});

  @override
  List<Object> get props => [categorys];

  @override
  String toString() => 'CategorysUpdate';
}
