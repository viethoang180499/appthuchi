import 'dart:async';
import 'package:app_thu_chi/blocs/categorys_bloc/categorys.dart';
import 'package:app_thu_chi/repository/categoryRepo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategorysBloc extends Bloc<CategorysEvent, CategorysState> {
  final CategoryRepository categoryRepo;
  StreamSubscription _categorySubcription;

  CategorysBloc({@required this.categoryRepo}) : assert(categoryRepo != null);

  @override
  CategorysState get initialState => CategorysLoading();

  @override
  Stream<CategorysState> mapEventToState(CategorysEvent event) async* {
    if (event is CategorysLoad) {
      yield* _mapCategorysLoadToState();
    } else if (event is CategoryAdded) {
      yield* _mapCategoryAddedToState(event);
    } else if (event is CategoryUpdated) {
      yield* _mapCategoryUpdatedToState(event);
    } else if (event is CategoryDeleted) {
      yield* _mapCategoryDeletedToState(event);
    } else if (event is CategorysUpdate) {
      yield* _mapCategoryUpdateToState(event);
    }
  }

  Stream<CategorysState> _mapCategorysLoadToState() async* {
    _categorySubcription?.cancel();
    _categorySubcription = categoryRepo.categorys().listen(
          (categorys) => add(
            CategorysUpdate(categorys: categorys),
          ),
        );
  }

  Stream<CategorysState> _mapCategoryAddedToState(CategoryAdded event) async* {
    categoryRepo.addNewCategory(event.category);
  }

  Stream<CategorysState> _mapCategoryUpdatedToState(
      CategoryUpdated event) async* {
    categoryRepo.updateCategory(event.category);
  }

  Stream<CategorysState> _mapCategoryDeletedToState(
      CategoryDeleted event) async* {
    categoryRepo.deleteCategory(event.category);
  }

  Stream<CategorysState> _mapCategoryUpdateToState(
      CategorysUpdate event) async* {
    yield CategorysLoadSuccess(categorys: event.categorys);
  }

  @override
  Future<void> close() {
    _categorySubcription?.cancel();
    return super.close();
  }
}
