import 'package:app_thu_chi/models/exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';


abstract class UpdateExchangeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ValueExchangeChanged extends UpdateExchangeEvent {
  final String name;
  final String note;
  final ExchangeType exchangeType;
  final String categoryId;
  final String image;
  final int amount;
  final DateTime date;

  ValueExchangeChanged(
      {this.name,
      this.note,
      this.exchangeType,
      this.categoryId,
      this.image,
      this.amount,
      this.date});

  @override
  List<Object> get props =>
      [name, note, exchangeType, categoryId, image, amount, date];

  @override
  String toString() =>
      'ValueChange: name: $name, note: $note, amount: $amount, date: $date';
}

class LoadExchange extends UpdateExchangeEvent {
  final String exchangeId;
  
  LoadExchange(this.exchangeId);
  List<Object> get props => [exchangeId];
  
  @override
  String toString() => 'Load exchange:';
}

class SubmitedChanges extends UpdateExchangeEvent {
  final bool isAdding;
  final Exchange exchange;

  SubmitedChanges({@required this.isAdding, @required this.exchange});
}
