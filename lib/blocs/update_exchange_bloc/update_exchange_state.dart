import 'package:app_thu_chi/utils/enums.dart';
import 'package:equatable/equatable.dart';

class UpdatedExchangeState extends Equatable {
  final String name;
  final String note;
  final ExchangeType exchangeType;
  final String categoryId;
  final String image;
  final int amount;
  final DateTime date;
  final bool isSuccess;

  UpdatedExchangeState(
      {this.name,
      this.note,
      this.exchangeType,
      this.categoryId,
      this.image,
      this.amount,
      this.date,
      this.isSuccess});

  UpdatedExchangeState updatedExchange(
      {String name,
      String note,
      ExchangeType exchangeType,
      String categoryId,
      String image,
      int amount,
      DateTime date,
      bool isSuccess}) {
    return copyWith(
        name, note, exchangeType, categoryId, image, amount, date, isSuccess);
  }

  UpdatedExchangeState updateExchangeSuccess() {
    return copyWith(this.name, this.note, this.exchangeType, this.categoryId,
        this.image, this.amount, this.date, true);
  }

  UpdatedExchangeState updateExchangeFailure() {
    return copyWith(this.name, this.note, this.exchangeType, this.categoryId,
        this.image, this.amount, this.date, false);
  }

  UpdatedExchangeState copyWith(
      String name,
      String note,
      ExchangeType exchangeType,
      String categoryId,
      String image,
      int amount,
      DateTime date,
      bool isSuccess) {
    return UpdatedExchangeState(
        name: name ?? this.name,
        note: note ?? this.note,
        exchangeType: exchangeType ?? this.exchangeType,
        categoryId: categoryId ?? this.categoryId,
        image: image ?? this.image,
        amount: amount ?? this.amount,
        date: date ?? this.date,
        isSuccess: isSuccess ?? this.isSuccess);
  }

  @override
  List<Object> get props =>
      [name, note, exchangeType, categoryId, image, amount, date];

  @override
  String toString() =>
      'UpdatedExchange: name: $name, note: $note, amount: $amount, date: $date';
}

class ExchangeLoading extends UpdatedExchangeState {}
