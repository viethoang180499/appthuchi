import 'package:app_thu_chi/blocs/bloc.dart';
import 'package:app_thu_chi/blocs/update_exchange_bloc/update_exchange.dart';
import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UpdateExchangeBloc
    extends Bloc<UpdateExchangeEvent, UpdatedExchangeState> {
  final ExchangesBloc exchangesBloc;

  UpdateExchangeBloc({@required this.exchangesBloc});

  @override
  UpdatedExchangeState get initialState => ExchangeLoading();

  @override
  Stream<UpdatedExchangeState> mapEventToState(
      UpdateExchangeEvent event) async* {
    if (event is LoadExchange) {
      yield* _mapLoadExchangeToState(event);
    } else if (event is ValueExchangeChanged) {
      yield* _mapValueChangedToState(event);
    } else {
      yield* _mapSubmitedChangesToState(event);
    }
  }

  Stream<UpdatedExchangeState> _mapLoadExchangeToState(
      LoadExchange event) async* {
    if (event.exchangeId != null) {
      final exchange = (exchangesBloc.state as ExchangesLoadSuccess)
          .exchanges
          .firstWhere((exchange) => exchange.id == event.exchangeId);
      yield state.updatedExchange(
          name: exchange.name,
          note: exchange.note,
          exchangeType: exchange.exchangeType,
          categoryId: exchange.categoryId,
          image: exchange.image,
          amount: exchange.amount,
          date: exchange.date);
    } else {
      yield state.updatedExchange(
        name: '',
        note: '',
        exchangeType: ExchangeType.income,
        categoryId: 'HfcwItAyGq9unA5i9A7z',
        amount: 0,
        date: DateTime.now(),
        image: '',
      );
    }
  }

  Stream<UpdatedExchangeState> _mapValueChangedToState(
      ValueExchangeChanged event) async* {
    yield state.updatedExchange(
        name: event.name,
        note: event.note,
        exchangeType: event.exchangeType,
        categoryId: event.categoryId,
        image: event.image,
        amount: event.amount,
        date: event.date);
  }

  Stream<UpdatedExchangeState> _mapSubmitedChangesToState(
      SubmitedChanges event) async* {
    try {
      if (event.isAdding) {
        exchangesBloc.add(ExchangeAdded(exchange: event.exchange));
      } else {
        exchangesBloc.add(ExchangeUpdated(exchange: event.exchange));
      }
      yield state.updateExchangeSuccess();
    } catch (e) {
      yield state.updateExchangeFailure();
    }
  }
}
