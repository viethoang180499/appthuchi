import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Localization {
  Locale locale;

  Localization(this.locale);

  void setLocale(Locale locale) {
    this.locale = locale;
  }

  static Localization of(BuildContext context) {
    return Localizations.of<Localization>(context, Localization);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'exchangesTitle': 'Exchanges',
      'statsTitle': 'Stats',
      'tabExchangesName': 'Exchanges',
      'tabStatsName': 'Stats',
      'incomes': 'Incomes',
      'expenses': 'Expenses',
      'all': 'All',
      'notiDelSuccess': 'Deleted exchange successful',
      'labelDelExchangeSnackBar': 'Undo',
      'overrallChartName': 'Overrall Chart',
      'specificChartName': 'Specific Chart',
      'saving': 'Saving: ',
      'from': 'From: ',
      'to': 'To: ',
      'filterBarHintText': 'Filter by name...',
      'addFabTooltip': 'Add exchange',
      'detailScreenTitle': 'Exchange detail',
      'noImage': 'No Image',
      'amountLable': 'Amount',
      'currency': '€',
      'addScreenTitle': 'Add Exchange',
      'editScreenTitle': 'Edit Exchange',
      'nameLable': 'Name',
      'noteLable': 'Note',
      'validate': 'Please fill it',
      'addCateLable': 'Category Name',
      'okBtnText': 'OK',
      'cancelBtnText': 'CANCEL',
      'english': 'English',
      'vietnamese': 'Vietnamese'
    },
    'vi': {
      'exchangesTitle': 'Danh sách giao dịch',
      'statsTitle': 'Thống kê',
      'tabExchangesName': 'Giao dịch',
      'tabStatsName': 'Thống kê',
      'incomes': 'Thu',
      'expenses': 'Chi',
      'all': 'Tất cả',
      'notiDelSuccess': 'Xóa giao dịch thành công',
      'labelDelExchangeSnackBar': 'Hoàn tác',
      'overrallChartName': 'Biểu đồ tổng quát',
      'specificChartName': 'Biểu đồ chi tiết',
      'saving': 'Tích lũy: ',
      'from': 'Từ ngày: ',
      'to': 'Đến ngày: ',
      'filterBarHintText': 'Tìm kiếm theo tên...',
      'addFabTooltip': 'Thêm mới giao dịch',
      'detailScreenTitle': 'Chi tiết giao dịch',
      'noImage': 'Không có hình ảnh',
      'amountLable': 'Số tiền',
      'currency': 'đ',
      'addScreenTitle': 'Thêm giao dịch',
      'editScreenTitle': 'Chỉnh sửa giao dịch',
      'nameLable': 'Tên',
      'noteLable': 'Ghi chú',
      'validate': 'Không để trống trường này',
      'addCateLable': 'Tên thể loại',
      'okBtnText': 'Đồng ý',
      'cancelBtnText': 'Hủy bỏ',
      'english': 'Tiếng Anh',
      'vietnamese': 'Tiếng Việt'
    },
  };

  String get english {
    return _localizedValues[locale.languageCode]['english'];
  }

  String get vietnamese {
    return _localizedValues[locale.languageCode]['vietnamese'];
  }

  String get okBtnText {
    return _localizedValues[locale.languageCode]['okBtnText'];
  }

  String get cancelBtnText {
    return _localizedValues[locale.languageCode]['cancelBtnText'];
  }

  String get addCateLable {
    return _localizedValues[locale.languageCode]['addCateLable'];
  }

  String get validate {
    return _localizedValues[locale.languageCode]['validate'];
  }

  String get nameLable {
    return _localizedValues[locale.languageCode]['nameLable'];
  }

  String get noteLable {
    return _localizedValues[locale.languageCode]['noteLable'];
  }

  String get addScreenTitle {
    return _localizedValues[locale.languageCode]['addScreenTitle'];
  }

  String get editScreenTitle {
    return _localizedValues[locale.languageCode]['editScreenTitle'];
  }

  String get currency {
    return _localizedValues[locale.languageCode]['currency'];
  }

  String get amountLable {
    return _localizedValues[locale.languageCode]['amountLable'];
  }

  String get noImage {
    return _localizedValues[locale.languageCode]['noImage'];
  }

  String get detailScreenTitle {
    return _localizedValues[locale.languageCode]['detailScreenTitle'];
  }

  String get addFabTooltip {
    return _localizedValues[locale.languageCode]['addFabTooltip'];
  }

  String get filterBarHintText {
    return _localizedValues[locale.languageCode]['filterBarHintText'];
  }

  String get from {
    return _localizedValues[locale.languageCode]['from'];
  }

  String get to {
    return _localizedValues[locale.languageCode]['to'];
  }

  String get saving {
    return _localizedValues[locale.languageCode]['saving'];
  }

  String get specificChartName {
    return _localizedValues[locale.languageCode]['specificChartName'];
  }

  String get overrallChartName {
    return _localizedValues[locale.languageCode]['overrallChartName'];
  }

  String get labelDelExchangeSnackBar {
    return _localizedValues[locale.languageCode]['labelDelExchangeSnackBar'];
  }

  String get notiDelSuccess {
    return _localizedValues[locale.languageCode]['notiDelSuccess'];
  }

  String get exchangesTitle {
    return _localizedValues[locale.languageCode]['exchangesTitle'];
  }

  String get statsTitle {
    return _localizedValues[locale.languageCode]['statsTitle'];
  }

  String get tabExchangesName {
    return _localizedValues[locale.languageCode]['tabExchangesName'];
  }

  String get tabStatsName {
    return _localizedValues[locale.languageCode]['tabStatsName'];
  }

  String get incomes {
    return _localizedValues[locale.languageCode]['incomes'];
  }

  String get expenses {
    return _localizedValues[locale.languageCode]['expenses'];
  }

  String get all {
    return _localizedValues[locale.languageCode]['all'];
  }
}

class LocalizationBlocDelegate extends LocalizationsDelegate<Localization> {
  const LocalizationBlocDelegate();
  @override
  bool isSupported(Locale locale) => ['en', 'vi'].contains(locale.languageCode);

  @override
  Future<Localization> load(Locale locale) =>
      SynchronousFuture<Localization>(Localization(locale));

  @override
  bool shouldReload(LocalizationsDelegate<Localization> old) => false;
}
