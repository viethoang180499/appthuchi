import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LocalizationEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LocalizationChanged extends LocalizationEvent{
  final Locale locale;

  LocalizationChanged({@required this.locale});
}