import 'dart:async';
import 'package:app_thu_chi/blocs/localization_bloc/localization_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'localization.dart';

class LocalizationBloc extends Bloc<LocalizationEvent, Localization> {
  @override
  Localization get initialState => Localization(Locale('en', ''));

  @override
  Stream<Localization> mapEventToState(LocalizationEvent event) async* {
    if (event is LocalizationChanged) {
      yield Localization(event.locale);
    }
  }
}
