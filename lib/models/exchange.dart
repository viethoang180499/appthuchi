import 'package:app_thu_chi/utils/enums.dart';
import 'package:flutter/widgets.dart';
import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Exchange extends Equatable {
  final String id;
  final String name;
  final ExchangeType exchangeType;
  final String categoryId;
  final int amount;
  final DateTime date;
  final String note;
  final String image;

  Exchange({
    @required this.name,
    @required this.exchangeType,
    @required this.categoryId,
    @required this.amount,
    @required this.date,
    String note,
    String image,
    String id,
  })  : this.id = id ?? Uuid().v4(),
        this.note = note ?? '',
        this.image = image ?? '';

  @override
  List<Object> get props =>
      [id, name, exchangeType, categoryId, amount, date, note, image];

  Exchange copyWith(
          {String id,
          String name,
          ExchangeType exchangeType,
          String categoryId,
          int amount,
          DateTime date,
          String note,
          String image}) =>
      Exchange(
        name: name ?? this.name,
        exchangeType: exchangeType ?? this.exchangeType,
        categoryId: categoryId ?? this.categoryId,
        amount: amount ?? this.amount,
        date: date ?? this.date,
        note: note ?? this.note,
        image: image ?? this.image,
        id: id ?? this.id,
      );

  static Exchange fromSnapshot(DocumentSnapshot snapshot) => Exchange(
        name: snapshot['name'],
        exchangeType: ExchangeType.values[snapshot['type']],
        categoryId: snapshot['categoryId'],
        amount: snapshot['amount'],
        date: (snapshot['date'] as Timestamp).toDate(),
        note: snapshot['note'],
        image: snapshot['image'],
        id: snapshot.documentID,
      );

  Map<String, Object> toDocument() => {
        'name': name,
        'type': exchangeType.index,
        'categoryId': categoryId,
        'amount': amount,
        'date': Timestamp.fromDate(date),
        'note': note,
        'image': image,
      };

  @override
  String toString() =>
      'transaction: id: $id, name: $name, exchangeType: $exchangeType, category: $categoryId, amount:  $amount, date: $date';
}
