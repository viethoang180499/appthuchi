import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class Category extends Equatable {
  final String id;
  final String name;

  Category({String id, @required this.name}) : this.id = id ?? Uuid().v4();

  Category copyWith({String id, String name}) =>
      Category(name: name ?? this.name, id: id ?? this.id);

  static Category fromSnapshot(DocumentSnapshot snapshot) =>
      Category(name: snapshot['name'], id: snapshot.documentID);

  Map<String, Object> toDocument() => {
        'id': id,
        'name': name,
      };

  @override
  List<Object> get props => [id, name];

  @override
  String toString() => 'Category: id: $id, name: $name';
}
